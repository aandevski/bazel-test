To build:
```
bazel build //crawler:run

INFO: Analyzed target //crawler:run (6 packages loaded, 16 targets configured).
INFO: Found 1 target...
Target //crawler:run up-to-date:
  bazel-bin/crawler/run
INFO: Elapsed time: 0.438s, Critical Path: 0.01s
INFO: 0 processes.
INFO: Build completed successfully, 1 total action
```

Executing:
```
./bazel-bin/crawler/run

Setec parser printing
Print from http helper
Printing from db client
```

