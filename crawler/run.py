import crawler.parsers.setec as setec
import common.db_client as db_client
import pydantic

setec.print_setec_parser()
db_client.print_from_db_client()
print(pydantic.version.VERSION)